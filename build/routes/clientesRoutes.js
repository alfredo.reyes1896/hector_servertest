"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var clientesController_1 = require("../controllers/clientesController");
var ClientesRoutes = /** @class */ (function () {
    function ClientesRoutes() {
        this.router = express_1.Router();
        this.config();
    }
    ClientesRoutes.prototype.config = function () {
        this.router.post("/create", clientesController_1.clienteController.create);
    };
    return ClientesRoutes;
}());
var clientesRoutes = new ClientesRoutes();
exports.default = clientesRoutes.router;
