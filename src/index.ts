import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';


import clientesRoutes from "./routes/clientesRoutes";


const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');


class Server {

    public app: Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();



    } 

    config(): void {
          this.app.set('port', process.env.PORT || 3000);
        //para debugear en la consola de la terminal las peticiones al servidor. 
        this.app.use(morgan('dev')); 

        this.app.use(cors());
 
        //para hacer solicitudes en formato JSON 
        this.app.use(express.json());

        //para enviar informaci��n desde un formulario HTML
        this.app.use(express.urlencoded({ extended: false }));



    }

    routes(): void {
        this.app.use("/clientes", clientesRoutes);
       

    }
    
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('listening on port '+this.app.get('port'));
        })
    }


}

const server = new Server();
server.start();
