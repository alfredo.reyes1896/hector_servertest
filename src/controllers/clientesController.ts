import { Request, Response } from "express";
import db from "../database";

class ClienteController{
    public async create(req: Request, res: Response) {
        try {
        
            const resp = await db.query(
                "INSERT INTO clientes SET ? ",
                req.body
            );
            return res.json(resp);
            
        } catch (e) {
          console.log(e); // [Error]
          res.status(500).send(e["code"]);
        }
      }
}

export const clienteController = new ClienteController();
