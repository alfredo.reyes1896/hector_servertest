import { Router } from "express";
import { clienteController } from "../controllers/clientesController";


class ClientesRoutes{
    public router: Router = Router();

    constructor() {
      this.config();
    }

    config(): void {
        this.router.post("/create", clienteController.create);
      }

}


const clientesRoutes = new ClientesRoutes();
export default clientesRoutes.router;
